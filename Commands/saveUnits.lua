function getInfo()
	return {
		tooltip = "Saves the units",
        onNoUnits = SUCCESS,
        parameterDefs = {
			{ 
				name = "paths",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            },
            { 
				name = "reversedPaths",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            },
            { 
				name = "assignment",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            },
            { 
				name = "positionsAssignment",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            },
            { 
				name = "canyonPoint",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            },
            { 
				name = "safeAreaCenter",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            },
            { 
				name = "transporters",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            }
		}
	}
end

local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitIsTransporting = Spring.GetUnitIsTransporting
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetUnitIsDead = Spring.GetUnitIsDead
local SpringEcho = Spring.Echo

-- Checks if table tbl contains a given value
function tableContainsValue(tbl, value)
    if tbl == nil then
        return false
    end
    
    for i = 1, #tbl do
        if tbl[i] == value then
            return true
        end
    end
    return false
end

-- Moves unit to a given position
function moveUnitToPosition(unitId, position)
    SpringGiveOrderToUnit(unitId, CMD.MOVE, position:AsSpringVector(), {})
end

-- Moves unit along a given (safe) path
function moveAlongPath(unitId, safePath)
    for i=1, #safePath do
        SpringGiveOrderToUnit(unitId, CMD.MOVE, (safePath[i]):AsSpringVector(), {"shift"})
    end
end

-- Performs initialization of members
function Initialize(self)
    self.unitsTransported = {}
    self.currentStates = {}
    self.pathEnds = {}
    self.unloadPositionX = 0
    self.unloadPositionZ = 0
    self.emptyUnloadPosition = {}
    self.transporters = nil
end

-- Transitions the unit to the given state
function transitionToState(self, transporterId, newState)
    self.currentStates[transporterId] = newState
end

-- Returns index (relative to unitsToTransport) of next unit that should be transporter by a given transporter
function getIndexOfUnitToTransport(self, transporterId, unitsToTransport)
    if self.unitsTransported[transporterId] == nil then
        self.unitsTransported[transporterId] = 1
    end

    local indexOfUnitToTransport = self.unitsTransported[transporterId]

    if indexOfUnitToTransport <= #unitsToTransport then
        -- this transporter is not done yet with transporting the assigned units
        return indexOfUnitToTransport
    else
        return -1
    end
end

-- Checks if the unit is loaded within the specified transporter
function isUnitLoaded(transporterId, unitId)
    local tbl = SpringGetUnitIsTransporting(transporterId)
    return tableContainsValue(tbl, unitId)
end

-- Checks if the transporter is empty (it does not have any units loaded)
function isTransporterEmpty(transporterId)
    local tbl = SpringGetUnitIsTransporting(transporterId)
    return tbl == nil or #tbl == 0
end

-- Loads unit to the given transporter
function loadUnit(transporterId, unitId)
    local tbl = SpringGetUnitIsTransporting(transporterId)
    if tableContainsValue(tbl, unitId) == false then
        SpringGiveOrderToUnit(transporterId, CMD.LOAD_UNITS, {unitId}, {})
    end
end

-- Returns position where next unit should be unloaded
-- The positions are selected so that it creates a grid in a square inscribed inside safe area
function getPositionForUnload(self, targetPosition, radius)
    if self.unloadPositionX == 0 then
        -- When called first time
        self.safeAreaSquareSize = radius * 1.41 -- 1.41 approximates (underestimates sqrt(2)), I am basically creating square inscribed into the safe area circle
        self.safeAreaSquareTopLeftCorner = Vec3(targetPosition.x - self.safeAreaSquareSize / 2, targetPosition.y, targetPosition.z - self.safeAreaSquareSize / 2)
        self.safeAreaSquareBottomRight = Vec3(targetPosition.x + self.safeAreaSquareSize / 2, targetPosition.y, targetPosition.z + self.safeAreaSquareSize / 2)
        self.unloadPositionSize = 110
        self.unloadPositionX = self.safeAreaSquareTopLeftCorner.x + self.unloadPositionSize / 2
        self.unloadPositionZ = self.safeAreaSquareTopLeftCorner.z + self.unloadPositionSize / 2
    end
    
    local position = Vec3(self.unloadPositionX, targetPosition.y, self.unloadPositionZ)
    
    if self.unloadPositionX + self.unloadPositionSize + self.unloadPositionSize / 2 <= self.safeAreaSquareBottomRight.x then
        -- If it fits into current row
        self.unloadPositionX = self.unloadPositionX + self.unloadPositionSize
    else
        -- If it does not fit into current row, move onto the next row
        self.unloadPositionX = self.safeAreaSquareTopLeftCorner.x + self.unloadPositionSize / 2
        self.unloadPositionZ = self.unloadPositionZ + self.unloadPositionSize
    end

    return position
end

-- Unloads the unit from the given transporter
function unloadUnit(self, transporterId, targetPosition, radius)
    local p = getUnitPosition(transporterId)
    local h = Spring.GetGroundHeight(p.x, p.z)
    SpringGiveOrderToUnit(transporterId, CMD.UNLOAD_UNIT, {p.x, h, p.z}, {})
    --SpringGiveOrderToUnit(transporterId, CMD.UNLOAD_UNIT, {targetPosition.x, targetPosition.y, targetPosition.z}, {})
end

-- Called whenever the given transporter saves some unit
function unitWasSaved(self, transporterId)
    -- Next time, next unit will be transported
    self.unitsTransported[transporterId] = self.unitsTransported[transporterId] + 1
end

-- Called whenever the given transporter was killed
function transporterWasKilled(transporterId, transporters, assignment)
    -- Reassign the units to alive transporters (uniformly)
    local transporterIndex = 1
    local anyTransporterAlive = false
    for i=1, #assignment[transporterId] do
        local unitId = assignment[transporterId][i]
        if SpringGetUnitIsDead(unitId) == false then
            local transpId = transporters[transporterIndex]
            if SpringGetUnitIsDead(transpId) == false then
                -- It is alive, so we give one unit to it
                local assign = assignment[transpId]
                assign[#assign + 1] = unitId
                anyTransporterAlive = true
            end
            if transporterIndex >= #transporters then
                if anyTransporterAlive == false then
                    return false
                else
                    transporterIndex = 1
                end
            else
                transporterIndex = transporterIndex + 1
            end
        end
    end
    
    assignment[transporterId] = {}
    return true
end

-- Returns a position of the given unit
function getUnitPosition(unitId)
    local pointX, pointY, pointZ = SpringGetUnitPosition(unitId)
    return Vec3(pointX, pointY, pointZ)
end

-- Checks whether the given units reached its target position
function doesUnitReachedItsPosition(unitId, targetPosition, targetThreshold)
    if targetPosition == nil then
        return true
    end
    local unitPos = getUnitPosition(unitId)
    return targetPosition:Distance(unitPos) < targetThreshold
end

-- Returns last node in the path
function getPathEnd(path)
    if #path == 0 then
        return nil
    else
        return path[#path]
    end
end

-- Definition of the states
------------------------------
-- Initial state
local INITIAL_STATE = 0
-- Transporter is moving to the canyon (canyon's entrypoint)
local MOVING_TO_CANYON = 1
-- Transporter is going for the unit
local GOING_FOR_UNIT = 2
-- Transporter is loading the unit
local LOADING_UNIT = 3
-- Transporter is moving back through the canyon (with the unit)
-- or better to say that the transporter is moving back to the canyon's entrypoint
local TRANSPORTING_UNIT_THROUGH_CANYON = 4
-- Transporter is moving to the safe area
local TRANSPORTING_UNIT_TO_SAFE_AREA = 5
-- Transporter waits (to prevent sliding bug)
local WAITING = 6
-- Transporter is unloading the unit
local UNLOADING_UNIT = 7
------------------------------

function Run(self, units, parameter)

    if self.unitsTransported == nil then
        Initialize(self)
    end

    local assignment = parameter.assignment
    local paths = parameter.paths
    local reversedPaths = parameter.reversedPaths
    local safeAreaCenter = parameter.safeAreaCenter
    local canyonPoint = parameter.canyonPoint
    local positionsAssignment = parameter.positionsAssignment
    local transporters = parameter.transporters

    if self.transporters == nil then
        self.transporters = transporters
    end

    local targetThreshold = 500
    local targetThresholdWhenLoaded = 500

    local modifier = {}

    local finished = true

    local missionInfo = Sensors.core.MissionInfo()
    
    if missionInfo.score >= missionInfo.scoreForBonus then
        SpringEcho("Bonus score was reached")
        return SUCCESS
    end

    local safeAreaRadius = missionInfo.safeArea.radius
    
    for transporterId, unitsToTransport in pairs(assignment) do	
        
        local transporterFinished = false

        if SpringGetUnitIsDead(transporterId) ~= false then
            -- If transporter was killed (call the callback that should reassign all it's units)
            if transporterWasKilled(transporterId, transporters, assignment) == false then
                -- All transporters are dead so we should succeed immediately
                SpringEcho("All the transporters are dead")
                return SUCCESS
            end
            transporterFinished = true
        end

        if self.currentStates[transporterId] == nil then
            -- the transporter is in idle state (INITIAL_STATE), prior to running the saving command
            self.currentStates[transporterId] = INITIAL_STATE
            self.currentTargetUnit = {}
            self.waitTicks = {}
        end

        -- This command replicates a behavior of small tree (about 7 nodes)
        -- so that ALL the transporters can save units in parallel in a way, that the transporters do not wait
        -- until the other transporters finished saving current group of units
        -- that is, transporter T1 can be saving its third unit while transporter T2 is still working on saving its first unit.
        -- Command is done once all the units are saved or there are no transporters (all of them were killed)
        -- Dynamic re-assignment is implemented so that whenever a transporter responsible for saving a unit is killed, the unit is
        -- reassigned to different transporter.
        if self.currentStates[transporterId] == INITIAL_STATE then
            -- Initial state, the transporter should start by moving to the canyon
            moveUnitToPosition(transporterId, canyonPoint)
            -- Switch the transporter to the next state
            transitionToState(self, transporterId, MOVING_TO_CANYON)
        elseif self.currentStates[transporterId] == MOVING_TO_CANYON then
            if doesUnitReachedItsPosition(transporterId, canyonPoint, targetThreshold) then
                -- The transporter has reached its goal state (i.e. canyon entry point)
                local index = getIndexOfUnitToTransport(self, transporterId, unitsToTransport)
                if index > 0 then
                    -- Transporter still has some units to transport
                    finished = false
                    self.currentTargetUnit[transporterId] = index
                    -- Get path that leads the transporter to the safe path endpoint that is nearest to the current target unit
                    local path = paths[unitsToTransport[index]]
                    -- Move along that path
                    moveAlongPath(transporterId, path)
                    self.pathEnds[transporterId] = getPathEnd(path)
                    -- Switch to the next state
                    transitionToState(self, transporterId, GOING_FOR_UNIT)
                else
                    -- Otherwise transporter does not have any units to transport so it has finished it's work
                    transporterFinished = true
                end
            end
        elseif self.currentStates[transporterId] == GOING_FOR_UNIT then
            local currentTargetUnitIndex = self.currentTargetUnit[transporterId]
            if doesUnitReachedItsPosition(transporterId, self.pathEnds[transporterId], targetThreshold) then
                -- The transporter reached the safe path endpoint, so it should load the unit (it is save to perform regular move from safe path endpoint to the target unit)
                loadUnit(transporterId, assignment[transporterId][currentTargetUnitIndex])
                -- Switch to the loading unit state
                transitionToState(self, transporterId, LOADING_UNIT)
            end
        elseif self.currentStates[transporterId] == LOADING_UNIT then
            if isUnitLoaded(transporterId, assignment[transporterId][self.currentTargetUnit[transporterId]]) then
                -- The unit has been loaded so we should transport it back by first moving back through the canyon
                -- The part from unit position back to the safe path endpoint (endpoint of original, non-reversed path) is handled implicitly and is safe
                local path = reversedPaths[unitsToTransport[self.currentTargetUnit[transporterId]]]
                -- Move along that path
                moveAlongPath(transporterId, path)
                self.pathEnds[transporterId] = getPathEnd(path)
                -- Switch to the next state
                transitionToState(self, transporterId, TRANSPORTING_UNIT_THROUGH_CANYON)
            end
        elseif self.currentStates[transporterId] == TRANSPORTING_UNIT_THROUGH_CANYON then
            if doesUnitReachedItsPosition(transporterId, self.pathEnds[transporterId], targetThresholdWhenLoaded) then
                -- End point of the reversed path was reached, so the position where the unit should be unloaded have to be obtained
                self.emptyUnloadPosition[transporterId] = getPositionForUnload(self, safeAreaCenter, safeAreaRadius)
                -- And the transporter should be moved to that position
                moveUnitToPosition(transporterId, self.emptyUnloadPosition[transporterId])
                -- Switch to the next state
                transitionToState(self, transporterId, TRANSPORTING_UNIT_TO_SAFE_AREA)
            end
        elseif self.currentStates[transporterId] == TRANSPORTING_UNIT_TO_SAFE_AREA then
            if doesUnitReachedItsPosition(transporterId, safeAreaCenter, targetThresholdWhenLoaded) then
                -- The transporter is done with transporting the unit to the safe area so we should wait for a moment (avoiding the sliding bug)
                transitionToState(self, transporterId, WAITING)
            end
        elseif self.currentStates[transporterId] == WAITING then
            -- Wait for 10 ticks
            if self.waitTicks[transporterId] == nil then
               self.waitTicks[transporterId] = 1
            elseif self.waitTicks[transporterId] >= 20 then
                self.waitTicks[transporterId] = 0
                -- When the transporter has waited for 20 ticks, it is safe to unload the unit
                unloadUnit(self, transporterId, self.emptyUnloadPosition[transporterId], safeAreaRadius)
                -- Then switch to the unloading state
                transitionToState(self, transporterId, UNLOADING_UNIT)
            else
                -- Wait a little bit longer
                self.waitTicks[transporterId] = self.waitTicks[transporterId] + 1
            end
        elseif self.currentStates[transporterId] == UNLOADING_UNIT then
            -- If the transporter is empty then the unit being transporter was unloaded
            if isTransporterEmpty(transporterId) then
                -- Therefore the unit was saved
                unitWasSaved(self, transporterId)
                -- And the transporter should transition back to it's initial state (to be able to transport next unit)
                transitionToState(self, transporterId, INITIAL_STATE)
            end
        end

        finished = finished and transporterFinished
    end

    -- All the transporters finished their jobs, so the node should succeed
    if finished == true then
        SpringEcho("Finished")
        return SUCCESS
    end

    return RUNNING
end

-- Unloads all the units from all the transporters (each transporter unloads below itself)
function UnloadAllUnits(transporters)
    for i=1, #transporters do
        local transporterId = transporters[i]
        local targetPosition = getUnitPosition(transporterId)
        SpringGiveOrderToUnit(transporterId, CMD.TIMEWAIT, { 1000 }, CMD.OPT_SHIFT)
        SpringGiveOrderToUnit(transporterId, CMD.UNLOAD_UNITS, {targetPosition.x, targetPosition.y, targetPosition.z, 256}, CMD.OPT_SHIFT)
    end
end

function Reset(self)
    self.unitsTransported = nil -- trigger initialization next time
    UnloadAllUnits(self.transporters)
    return self
end