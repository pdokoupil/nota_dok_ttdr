local sensorInfo = {
	name = "getSafePathEndpoints",
	desc = "Returns safe path endpoints, i.e. the points where the safe path should end (target points)",
	author = "dok",
	date = "2020-06-07",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SpringGetUnitPosition = Spring.GetUnitPosition

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return list with safe path endpoints
return function(unitsTable, canyonEntryPoint)
    
    local endpoints = {}

    for i=1, #unitsTable["armrad"] do
        local pointX, pointY, pointZ = SpringGetUnitPosition(unitsTable["armrad"][i])
        local unitPos = Vec3(pointX, pointY, pointZ)
        endpoints[#endpoints + 1] = unitPos
    end

    endpoints[#endpoints + 1] = canyonEntryPoint

    return endpoints
end