local sensorInfo = {
	name = "reversePaths",
	desc = "Reverse the given paths",
	author = "dok",
	date = "2020-06-03",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function reverse(arr, arrLen)
    reversed = {}
    for i=1, arrLen do
        reversed[arrLen + 1 - i] = arr[i]
    end
    return reversed
end

-- @description return reversed paths
return function(paths)
    
    local reversedPaths = {}

    for unitId, path in pairs(paths) do
        reversedPaths[unitId] = reverse(path, #path - 1)
    end
    
    return reversedPaths
end