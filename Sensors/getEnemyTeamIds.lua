local sensorInfo = {
	name = "getEnemyTeamIds",
	desc = "Returns team ids of enemies",
	author = "dok",
	date = "2020-06-06",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SpringGetTeamList = Spring.GetTeamList
local SpringGetMyTeamID = Spring.GetMyTeamID
local SpringGetTeamUnits = Spring.GetTeamUnits

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return list with team ids of enemies
return function()
    
    local enemyTeamIds = {}

    local teamIds = SpringGetTeamList()
    local myTeamId = SpringGetMyTeamID()
    for teamIndex=1, #teamIds do
        local teamId = teamIds[teamIndex]
        if teamId ~= myTeamId then
            local teamUnits = SpringGetTeamUnits(teamId)
            if #teamUnits > 0 then
                enemyTeamIds[#enemyTeamIds + 1] = teamId
            end
        end
    end

    return enemyTeamIds
end