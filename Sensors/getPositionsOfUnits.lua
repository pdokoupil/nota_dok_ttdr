local sensorInfo = {
	name = "getPositionsOfUnits",
	desc = "Get positions of units in the assignment",
	author = "dok",
	date = "2020-06-03",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SpringGetUnitPosition = Spring.GetUnitPosition

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return positions of units in the assignment (mapping from unit ids to unit positions)
return function(assignment)
    
    local positionsAssignment = {}
    for transporterId, unitsToTransport in pairs(assignment) do
        positionsAssignment[transporterId] = {}
        for i=1, #unitsToTransport do
            local pointX, pointY, pointZ = SpringGetUnitPosition(unitsToTransport[i])
            local unitPos = Vec3(pointX, pointY, pointZ)
            positionsAssignment[transporterId][i] = unitPos
        end
    end

    return positionsAssignment

end