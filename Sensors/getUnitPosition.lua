local sensorInfo = {
	name = "getUnitPosition",
	desc = "Get position of a given unit",
	author = "dok",
	date = "2020-05-21",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SpringGetUnitPosition = Spring.GetUnitPosition

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return position of the unit
return function(unit)
    
    local x, y, z = SpringGetUnitPosition(unit)
    
    return Vec3(x, y, z)

end