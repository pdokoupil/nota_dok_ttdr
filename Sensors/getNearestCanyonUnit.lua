local sensorInfo = {
	name = "getNearestCanyonUnit",
	desc = "Get the nearest unit in canyon",
	author = "dok",
	date = "2020-06-03",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local GetGroundOrigHeight = Spring.GetGroundOrigHeight
local SpringGetMyTeamID = Spring.GetMyTeamID
local SpringGetTeamUnits = Spring.GetTeamUnits
local SpringGetUnitPosition = Spring.GetUnitPosition

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return the nearest unit in canyon
return function(startPosition)
    local mapWidth = Game.mapSizeX
    local mapHeight = Game.mapSizeZ
    
    local tileWidth = 256
    local tileHeight = 256

    startPosition.y = GetGroundOrigHeight(startPosition.x, startPosition.z)

    -- local canyonPoints = {}
    local nearestCanyonPoint = nil
    local nearestDistance = nil

    canyonThreshold = 100

    local myTeamId = SpringGetMyTeamID()
    local units = SpringGetTeamUnits(myTeamId)

    for i=1, #units do
        local x, y, z = SpringGetUnitPosition(units[i])
        if y <= canyonThreshold then
            local canyonPoint = Vec3(x, y, z)
            local distance = startPosition:Distance(canyonPoint)
            if nearestCanyonPoint == nil or distance < nearestDistance then
                nearestCanyonPoint = units[i] --canyonPoint
                nearestDistance = distance
            end
            --canyonPoints[i] = Vec3(x, groundHeight, y)
        end
    end

    return nearestCanyonPoint --canyonPoints

end