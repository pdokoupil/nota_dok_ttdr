local sensorInfo = {
	name = "assignUnitsToTransporters",
	desc = "Assigns units to transporters",
	author = "dok",
	date = "2020-06-03",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SpringGetUnitPosition = Spring.GetUnitPosition

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function isAlreadySaved(unitId, safeAreaCenter, safeAreaRadius)
    local pointX, pointY, pointZ = SpringGetUnitPosition(unitId)
    local unitPos = Vec3(pointX, pointY, pointZ)
    return unitPos:Distance(safeAreaCenter) <= safeAreaRadius
end

function concatenateListsWhereNotAlreadySaved(a, b, safeAreaCenter, safeAreaRadius)
    for i=1,#b do
        if isAlreadySaved(b[i], safeAreaCenter, safeAreaRadius) == false then
            a[#a + 1] = b[i]
        end
    end
end

function shouldUnitBeTransported(unitName)
    return unitName ~= "armbox" and
        unitName ~= "armmllt" and
        unitName ~= "armbull" and
        unitName ~= "armatlas" and
        unitName ~= "armwin" and
        unitName ~= "armpeep" and
        unitName ~= "armrad"
end

-- @description returns assignment of target positions for the transporters
return function(unitsTable, transporters, safeAreaCenter, safeAreaRadius)
    
    local assignment = {}

    -- ordered by the number of points awarded when saving the given unit
    local units = {}

    concatenateListsWhereNotAlreadySaved(units, unitsTable["armbox"], safeAreaCenter, safeAreaRadius) -- box of death is for 10 points
    concatenateListsWhereNotAlreadySaved(units, unitsTable["armmllt"], safeAreaCenter, safeAreaRadius) -- hattrack is for 5 points
    concatenateListsWhereNotAlreadySaved(units, unitsTable["armbull"], safeAreaCenter, safeAreaRadius) -- bulldog is for 2 points

    -- Then add the rest of units
    for key, value in pairs(unitsTable) do
        if shouldUnitBeTransported(key) then
            concatenateListsWhereNotAlreadySaved(units, value, safeAreaCenter, safeAreaRadius)
        end
    end
    
    local unitIndex = 1

    while unitIndex <= #units do
        for i=1, #transporters do
            local transporterId = transporters[i]
            if assignment[transporterId] == nil then
                assignment[transporterId] = {}
            end
            local transporterAssignment = assignment[transporterId]
            local unitId = units[unitIndex]
            transporterAssignment[#transporterAssignment + 1] = unitId
            unitIndex = unitIndex + 1
        end
    end

    return assignment
end