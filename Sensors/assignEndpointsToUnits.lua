local sensorInfo = {
	name = "assignEndpointsToUnits",
	desc = "Returns safe path endpoints, i.e. the points where the safe path should end (target points)",
	author = "dok",
	date = "2020-06-07",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SpringGetUnitPosition = Spring.GetUnitPosition

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return assignment of safe path endpoints
return function(assignment, endpoints, endpointDistanceThreshold)
    
    local endpointAssignment = {}

    for transporterId, unitsToTransport in pairs(assignment) do
        for i=1, #unitsToTransport do
            local unitId = unitsToTransport[i]
            local pointX, pointY, pointZ = SpringGetUnitPosition(unitId)
            local unitPos = Vec3(pointX, pointY, pointZ)
            for j=1, #endpoints do
                if unitPos:Distance(endpoints[j]) <= endpointDistanceThreshold then
                    endpointAssignment[unitId] = j
                end
            end
            if endpointAssignment[unitId] == nil then
                endpointAssignment[unitId] = -1 -- no endpoint for a given unit
            end
        end
    end

    return endpointAssignment

end