local sensorInfo = {
	name = "getSafePath",
	desc = "Creates a safe path from source to destination",
	author = "dok",
	date = "2020-06-03",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local GetGroundOrigHeight = Spring.GetGroundOrigHeight
local SpringGetUnitsInSphere = Spring.GetUnitsInSphere
local SpringGetUnitPosition = Spring.GetUnitPosition


function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function getNodeIndex(nodeX, nodeZ, mapWidth, tileSize)
    local nodeTileX = getNodeTileX(nodeX, tileSize)
    local nodeTileY = getNodeTileZ(nodeZ, tileSize)
    local mapWidthInTiles = (mapWidth + tileSize - 1) / tileSize
    return (nodeTileY - 1) * mapWidthInTiles + nodeTileX
end

function getNodeTileX(nodeX, tileSize)
    return (nodeX + tileSize - 1) / tileSize
end

function getNodeTileZ(nodeZ, tileSize)
    return (nodeZ + tileSize - 1) / tileSize
end

function clip(x, lower_bound, upper_bound)
    if x < lower_bound then
        return lower_bound
    elseif x > upper_bound then
        return upper_bound
    end
    return x
end

function getNeighbors(nodePosition, tileSize, mapWidth, mapHeight)
    local neighbors = {}
    
    neighbors[1] = Vec3(clip(nodePosition.x - tileSize, 1, mapWidth), 0, nodePosition.z) -- left neighbor
    neighbors[1].y = GetGroundOrigHeight(neighbors[1].x, neighbors[1].z)
    
    neighbors[2] = Vec3(clip(nodePosition.x + tileSize, 1, mapWidth), 0, nodePosition.z) -- right neighbor
    neighbors[2].y = GetGroundOrigHeight(neighbors[2].x, neighbors[2].z)

    neighbors[3] = Vec3(nodePosition.x, 0, clip(nodePosition.z - tileSize, 1, mapHeight)) -- top neighbor
    neighbors[3].y = GetGroundOrigHeight(neighbors[3].x, neighbors[3].z)

    neighbors[4] = Vec3(nodePosition.x, 0, clip(nodePosition.z + tileSize, 1, mapHeight)) -- bottom neighbor
    neighbors[4].y = GetGroundOrigHeight(neighbors[4].x, neighbors[4].z)
    return neighbors
end

function isEnemyUnitInSphere(teamIds, point, radius)

    for i=1, #teamIds do
        local units = SpringGetUnitsInSphere(point.x, point.y, point.z, radius, teamIds[i])
        if (#units > 0) then
            return true
        end
    end

    return false
end

function isNeighborSafe(teamIds, neighbor, cache, mapWidth, tileSize)

    local nodeIndex = getNodeIndex(neighbor.x, neighbor.z, mapWidth, tileSize)
    if cache[nodeIndex] ~= nil then
        return cache[nodeIndex]
    else
        local safety = false
        if neighbor.y <= 0 then
            safety = true
        elseif isEnemyUnitInSphere(teamIds, neighbor, 1100) then
            safety = false
        else
            safety = true
        end
        cache[nodeIndex] = safety
        return safety
    end
end

function filterSafeNeighbors(teamIds, neighbors, cache, mapWidth, tileSize)
    local safeNeighbors = {}
    for i=1, #neighbors do
        if isNeighborSafe(teamIds, neighbors[i], cache, mapWidth, tileSize) then
            safeNeighbors[#safeNeighbors + 1] = neighbors[i]
        end
    end

    return safeNeighbors
end

function buildPath(lastElementOnPath, parent, mapWidth, tileSize)
    local currentElementIndex = getNodeIndex(lastElementOnPath.x, lastElementOnPath.z, mapWidth, tileSize)
    
    local path = {lastElementOnPath}

    while parent[currentElementIndex] ~= nil do
        local parentElement = parent[currentElementIndex]
        path[#path + 1] = parentElement
        currentElementIndex = getNodeIndex(parentElement.x, parentElement.z, mapWidth, tileSize)
    end

    return reverse(path, #path - 1) -- pass the path length explicitly so that we get rid of the first (now last in the array) node in the path because that is where the unit currently stays
end

function reverse(arr, arrLen)
    local reversed = {}
    for i=1, arrLen do
        reversed[arrLen + 1 - i] = arr[i]
    end
    return reversed
end

function findPath(teamIds, from, to, mapWidth, mapHeight, tileSize, threshold, cache)
    local queue = {from}
    local first = 1
    local last = 2
    local parent = {}
    local fromNodeIndex = getNodeIndex(from.x, from.z, mapWidth, tileSize)

    while first < last do
        -- Take first element from the queue
        local firstElement = queue[first]
        first = first + 1

        -- If it is the (or is near ...) target node then we reconstruct and return the path
        if firstElement:Distance(to) < threshold then --firstElement.x == to.x and firstElement.z == to.z then
            return buildPath(firstElement, parent, mapWidth, tileSize)
        end

        -- Otherwise we all its neighbors (those that were not visited) to the queue
        local neighbors = getNeighbors(firstElement, tileSize, mapWidth, mapHeight)
        neighbors = filterSafeNeighbors(teamIds, neighbors, cache, mapWidth, tileSize)
        for i=1, #neighbors do
            local neigh = neighbors[i]
            local nodeIndex = getNodeIndex(neigh.x, neigh.z, mapWidth, tileSize)
            -- If unvisited node
            if parent[nodeIndex] == nil and nodeIndex ~= fromNodeIndex then
                parent[nodeIndex] = firstElement
                queue[last] = neigh
                last = last + 1
            end
        end
    end

    return nil

end

-- @description return safe path
return function(initialPosition, endpoints, endpointsAssignment, tileSize, enemyTeamIds)

    local mapWidth = Game.mapSizeX
    local mapHeight = Game.mapSizeZ

    local threshold = tileSize - 1

    local paths = {}

    local cache = {}

    local endpointPaths = {}

    for unitId, endpointIndex in pairs(endpointsAssignment) do
        if endpointIndex < 0 then
            local pointX, pointY, pointZ = SpringGetUnitPosition(unitId)
            local unitPos = Vec3(pointX, pointY, pointZ)
            paths[unitId] = findPath(enemyTeamIds, initialPosition, unitPos , mapWidth, mapHeight, tileSize, threshold, cache)
        else
            if endpointPaths[endpointIndex] == nil then
                endpointPaths[endpointIndex] = findPath(enemyTeamIds, initialPosition, endpoints[endpointIndex], mapWidth, mapHeight, tileSize, threshold, cache)
            end
            paths[unitId] = endpointPaths[endpointIndex]
        end
    end

    return paths
end
